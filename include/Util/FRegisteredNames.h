/*===========================================================================*\
|*  libunr - An open source Unreal Engine 1 implementation in library form   *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * FRegisteredNames.h - Hardcoded names used in libunr
 * 
 * Some names from UT v432 public headers
 * adapted to libunr by Adam 'Xaleros' Smith
 *========================================================================
*/

  xx( None )

  // Property types
  xx( ByteProperty )
  xx( IntProperty )
  xx( BoolProperty )
  xx( FloatProperty )
  xx( ObjectProperty )
  xx( NameProperty )

  xx( StringProperty )
  xx( ClassProperty )
  xx( ArrayProperty )
  xx( StructProperty )
  xx( VectorProperty )
  xx( RotatorProperty )
  xx( StrProperty )
  xx( MapProperty )
  xx( FixedArrayProperty )

  // Packages
  xx( Core )
  xx( Engine )
  xx( Editor )
  xx( UnrealI )
  xx( UnrealShare )
  xx( Fire )

  // UnrealScript types
  xx( Byte )
  xx( Int )
  xx( Bool )
  xx( Float )
  xx( Name )
  xx( String )
  xx( Struct )
  xx( Vector )
  xx( Rotator )
  xx( Color )
  xx( Plane )

  // Keywords
  xx( Begin )
  xx( State )
  xx( Function )
  xx( Self )
  xx( True )
  xx( False )
  xx( Transient )
  xx( Enum )
  xx( Replication )
  xx( Reliable )
  xx( Unreliable )
  xx( Always )

  // Object class names
  xx( Field )
  xx( Object )
  xx( TextBuffer )
  xx( Linker )
  xx( LinkerLoad )
  xx( LinkerSave )
  xx( Subsystem )
  xx( Factory )
  xx( TextBufferFactory )
  xx( Exporter )
  xx( StackNode )
  xx( Property )
  xx( Camera )
  xx( MD5Utils )

  // Constants
  xx( Vect )
  xx( Rot )
  xx( ArrayCount )
  xx( EnumCount )

  // Flow control
  xx( Else )
  xx( If )
  xx( Goto )
  xx( Stop )
  xx( Until )
  xx( While )
  xx( Do )
  xx( Break )
  xx( For )
  xx( ForEach )
  xx( Assert )
  xx( Switch )
  xx( Case )
  xx( Default )
  xx( Continue )

  // Variable overrides
  xx( Private )
  xx( Const )
  xx( Out )
  xx( Export )
  xx( Skip )
  xx( Coerce )
  xx( Optional )
  xx( Input )
  xx( Config )
  xx( Travel )
  xx( EditConst )
  xx( Localized )
  xx( GlobalConfig )
  xx( SafeReplace )
  xx( New )

  // Class overrides
  xx( Expands )
  xx( Intrinsic )
  xx( Within )
  xx( Abstract )
  xx( Package )
  xx( Guid )
  xx( Parent )
  xx( Class )
  xx( Extends )
  xx( NoExport )
  xx( NoUserCreate )
  xx( PerObjectConfig )
  xx( NativeReplication )

  // State overrides
  xx( Auto )
  xx( Ignores )

  // Calling overrides
  xx( Global )
  xx( Super )
  xx( Outer )

  // Function overrides
  xx( Operator )
  xx( PreOperator )
  xx( PostOperator )
  xx( Final )
  xx( Iterator )
  xx( Latent )
  xx( Return )
  xx( Singular )
  xx( Simulated )
  xx( Exec )
  xx( Event )
  xx( Static )
  xx( Native )
  xx( Invariant )

  // Variable declaration
  xx( Var )
  xx( Local )
  xx( Import )
  xx( From )

  // Special commands
  xx( Spawn )
  xx( Array )
  xx( Map )

  // Misc
  xx( Tag )
  xx( Role )
  xx( RemoteRole )
  xx( System )
  xx( User )
  xx( Template )
  xx( Pointer )
  xx( UTrace )
  xx( EditInLine )
  xx( AutoDestruct )

  // Log messages
  xx( Log )
  xx( Critical )
  xx( Init )
  xx( Exit )
  xx( Cmd )
  xx( Play )
  xx( Console )
  xx( Warning )
  xx( ExecWarning )
  xx( ScriptWarning )
  xx( ScriptLog )
  xx( Dev )
  xx( DevNet )
  xx( DevPath )
  xx( DevNetTraffic )
  xx( DevAudio )
  xx( DevLoad )
  xx( DevSave )
  xx( DevGarbage )
  xx( DevKill )
  xx( DevReplace )
  xx( DevMusic )
  xx( DevSound )
  xx( DevCompile )
  xx( DevBind )
  xx( Localization )
  xx( Compatibility )
  xx( NetComeGo )
  xx( Title )
  xx( Error )
  xx( Heading )
  xx( SubHeading )
  xx( FriendlyError )
  xx( Progress )
  xx( UserPrompt )
  xx( ServerLog )

  // Supported music format strings
  xx( It )
  xx( Xm )
  xx( S3M )
  xx( Stm )
  xx( Mod )
  xx( Ptm )
  xx( 669 )
  xx( Psm )
  xx( Mtm )
  xx( Riff )
  xx( Asy )
  xx( Amf )
  xx( Okt )
  xx( Mp3 )
  xx( Ogg )

  // Game Music Emu formats
  xx( Ay )   // ZX Spectrum
  xx( Gbs )  // Game Boy
  xx( Gym )  // Sega Genesis
  xx( Hes )  // NEC PC Engine / TurboGrafx-16
  xx( Kss )  // MSX / Other Z80
  xx( Nsf )  // NES
  xx( Nsfe ) // NES Extended
  xx( Sap )  // Atari SAP
  xx( Spc )  // SNES
  xx( Vgm )  // Sega VGM/VGZ